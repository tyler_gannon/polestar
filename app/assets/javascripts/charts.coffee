document.addEventListener 'turbolinks:load', ->
  $($('input[type=text], textarea')[0]).focus()

  $(document).on 'click', '.chart_display [data-palace-id]', (e) ->
    palace_id = $(e.target).parents('.palace').data('palace-id')
    $('.notes .palace-notes').addClass('hidden-xs-up')
    $('#notes-' + palace_id).removeClass('hidden-xs-up')

  $(document).on 'keyup', 'input#chart-search', (e) ->
    regexp = new RegExp(this.value, "i")
    if this.value.length <= 1
      $('.table-list .table-body .row').removeClass('hidden-xs-up')
    else
      $.each $('.table-list .table-body .row'), (index, object) ->
        $(object).toggleClass('hidden-xs-up', !regexp.test(object.dataset.search_text))
