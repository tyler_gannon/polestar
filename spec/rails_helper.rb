# frozen_string_literal: true
# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'

require 'devise'
require 'cancan/matchers'
require_relative 'support/capybara-webkit.config.rb'
require_relative 'support/capybara_helpers.rb'
# require_relative 'support/feature_spec_helpers.rb'
require 'database_cleaner'
# require 'vcr'

# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  # config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.include Devise::TestHelpers, :type => :controller
  # config.extend FeatureSpecHelpers, :type => :feature
  config.include FactoryGirl::Syntax::Methods
  config.infer_base_class_for_anonymous_controllers = false

  # Capybara.javascript_driver = :webkit
  Capybara.javascript_driver = :webkit_with_qt_plugin_messages_suppressed

  config.run_all_when_everything_filtered = true
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # VCR.configure do |config|
  #   config.cassette_library_dir = "support/cassettes"
  #   config.hook_into :webmock # or :fakeweb
  #   config.ignore_hosts 'api-ssl.bitly.com', '127.0.0.1'
  # end

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
  config.before(:suite) do
    ActiveRecord::Migration.maintain_test_schema!
    puts "Reseeding primary data tables"
    Rails.cache.clear if ENV['CACHE']
    [Stem, Branch, Palace, Pillar, Star].each &:reseed
  end

  config.before(:each) do
    OmniAuth.config.test_mode = true
  end

  EXCLUDED_TABLES = [
    ActiveRecord::InternalMetadata.table_name,
    'stems', 'branches', 'pillars', 'stars'
  ].freeze

  DatabaseCleaner.strategy = :truncation, {except: EXCLUDED_TABLES}
  config.before(:each) do
    DatabaseCleaner.clean_with :truncation, except: EXCLUDED_TABLES
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    # Choose a test framework:
    with.test_framework :rspec

    # Choose one or more libraries:
    with.library :active_record
    with.library :active_model
    with.library :action_controller
    # Or, choose the following (which implies all of the above):
    with.library :rails
  end
end
