require 'rails_helper'

RSpec.describe Chart, type: :model do
  it "should set the zi wei" do
    chart = create :chart
    # chart.build_chart!
    expect(chart.zi_wei_id).to eq 5
    expect(chart.ming_id).to eq 9

    # expect(chart.solar_date).to eq 1978-04-07 15:00:00 UTC
    expect(chart.year_id).to eq 55
    expect(chart.month_id).to eq 53
    expect(chart.day_id).to eq 36
    expect(chart.hour_id).to eq 9
    expect(chart.inner_element).to eq 'wood'
    expect(chart.lunar_year).to eq 4676
    expect(chart.lunar_month).to eq 3
    expect(chart.day_of_month).to eq 1
    expect(chart.ming_id).to eq 9
    expect(chart.zi_wei_id).to eq 5
    expect(chart.tian_fu_id).to eq 1
    expect(chart.tian_xiang_id).to eq 5
    expect(chart.tian_ji_id).to eq 4
    expect(chart.tai_yang_id).to eq 2
    expect(chart.wu_qu_id).to eq 1
    expect(chart.tai_yin_id).to eq 2
    expect(chart.ju_men_id).to eq 4
    expect(chart.tian_tong_id).to eq 12
    expect(chart.tian_liang_id).to eq 6
    expect(chart.wen_chang_id).to eq 3
    expect(chart.wen_qu_id).to eq 1
    expect(chart.lian_zhen_id).to eq 9
    expect(chart.tan_lang_id).to eq 3
    expect(chart.qi_sha_id).to eq 7
    expect(chart.po_jun_id).to eq 11
    expect(chart.huo_xing_id).to eq 10
    expect(chart.ling_xing_id).to eq 12
    expect(chart.yang_ren_id).to eq 7
    expect(chart.tuo_luo_id).to eq 5
    expect(chart.you_bi_id).to eq 9
    expect(chart.zuo_fu_id).to eq 7
    expect(chart.tian_cun_id).to eq 6
    expect(chart.tian_yao_id).to eq 4
    expect(chart.tian_kui_id).to eq 8
    expect(chart.tian_xi_id).to eq 4
    expect(chart.di_gong_id).to eq 4
    expect(chart.tian_yue_id).to eq 2
    expect(chart.tian_xing_id).to eq 12
    expect(chart.di_jie_id).to eq 8
    expect(chart.yi_ma_id).to be_nil
    expect(chart.wood_score).to eq 1
    expect(chart.fire_score).to eq 3
    expect(chart.earth_score).to eq 4
    expect(chart.metal_score).to eq 2
    expect(chart.water_score).to eq 2
    expect(chart.leap).to eq false
    expect(chart.lunar_month_number).to be_nil
  end
end
