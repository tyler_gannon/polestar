require 'rails_helper'

describe LunarDate do
  let(:solar_date) {DateTime.parse('Fri, 07 Apr 1978 15:00:00 +0000')}

  subject {LunarDate.from_solar_date solar_date}

  it "should have a month pillar" do
    expect(subject.month_pillar).not_to be_nil
  end

  it "should have a hour pillar" do
    expect(subject.hour_pillar).not_to be_nil
  end
end
