require 'rails_helper'

RSpec.describe StarRelationship, type: :model do
  let(:member) {create :member}
  def create_matching_relationship
    st = StarRelationship.create! palace: Palace.first
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:zi_wei), required: true
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:tian_fu), required: true
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:wen_qu), required: false
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:wu_qu), required: false
    Comment.create! member: member, comments: 'foobar', citation: 'nice', commentable: st
    st
  end

  def create_unmatching_relationship
    st = StarRelationship.create! palace: Palace.first
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:zi_wei), required: true
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:tian_fu), required: false
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:wen_qu), required: false
    st.star_relationship_stars.create! star: Star.find_by_pinyin(:wu_qu), required: false
    Comment.create! member: member, comments: 'foobar', citation: 'nice', commentable: st
    st
  end

  subject {create_matching_relationship}

  before do
    2.times {create_matching_relationship}
    create_unmatching_relationship
  end
  it "is able to find matching shit" do
    expect(subject.matches).to have(2).items
  end
  it "is able to merge stuff" do
    subject.merge(subject.matches)
    subject.reload
    expect(subject.comments).to have(3).items
  end
end
