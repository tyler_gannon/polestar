FactoryGirl.define do
  factory :chart do
    member
    dob {
      Time.use_zone('America/Los_Angeles') do
        Time.zone.parse('1978-04-07 15:30')
      end
    }
    time_zone 'America/Los_Angeles'
  end
end
