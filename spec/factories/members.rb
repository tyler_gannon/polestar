FactoryGirl.define do
  factory :member do
    first_name 'Foo'
    last_name 'Bar'
    email {SecureRandom.uuid + "@nowhere.com"}
    password {SecureRandom.uuid}
  end
end
